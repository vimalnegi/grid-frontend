console.log('loaded');

window.onload = function name(params) {
  initialize();

};
class View {
  
  constructor({srcs = []}={srcs:[]}) {
    this.srcs = srcs;
    this.gridImageItems = '';
    this.gridImageContainer='';
    this.updateContainer();
  };

  //private
  generateGridImageContainer(content= this.content) {
    return `<div id="gridContainer" class="grid-container">${content}</div`;
  }

  updateContainer() {
    let content = this.generateGridImageItems(this.srcs);
    this.content = content;
    this.value = this.generateGridImageContainer(content);
    return this;
  }

  //private
  generateGridImageItem(imageSrc){
    return `<img class="grid-item" src="${imageSrc}"></img>`;
  }

  addImages(imageSrcs) {
    this.content += this.generateGridImageItems(imageSrcs); 
    this.value = this.generateGridImageContainer();
    return this;
  }

  //private
  generateGridImageItems(imageSrcs) {
    return imageSrcs.map(imageSrc => this.generateGridImageItem(imageSrc)).join('\n');
  }

}

function getImages(page){
  return httpCall({
    type: "GET",
    url: `http://tapdiscover.com:9000/design-suggestions/?category=lehenga&page=${page}`
  })
    .then(data => {
      data = JSON.parse(data);
      let images = data.results;
      let imageUrls = images.map(image => image.image_link);
      return imageUrls;
    })
}

function initialize(){
  var a = document.getElementById('__parent__');
  var view = new View();
  var pageNo=1;
  getImages(pageNo)
  .then(imageUrls => updateImages.bind({a, view})(imageUrls))

  window.onscroll = function (ev) {
    if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
      getImages(++pageNo)
      .then(imageUrls => updateImages.bind({a, view})(imageUrls))
    }
  };
}

function updateImages(imageUrls){
  this.view.addImages(imageUrls);
  this.a.innerHTML = this.view.value;
}

function httpCall({ type, url }) {
  return new Promise((resolve, reject) => {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
        return resolve(this.responseText)
      }
      else if (this.readyState == 4) {
        return reject({status: this.status})
      }
    };
    xhttp.open(type, url, true);
    xhttp.send();
  });
}



